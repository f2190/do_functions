# import pandas as pd
# from datetime import datetime
# from sqlalchemy import create_engine  # , text
# import psycopg2
# import os


def main(args):
    tables = ['auth_user',
              'beyondyield_project', 'beyondyield_project_schedule',
              'beyondyield_project_observation', 'beyondyield_project_econ_data',
              'beyondyield_heckles', 'beyondyield_sms']
    dfs = ['df_' + x for x in tables]
    #
    # try:
    #
    #     db_url = os.getenv('DATABASE_URL')
    #     engine = create_engine(db_url)  # Create an SQLAlchemy engine
    #
    #     for table, df_name in zip(tables, dfs):
    #         query = f"SELECT * FROM {table};"
    #         globals()[df_name] = pd.read_sql(query, engine)  # Use pd.read_sql() with the SQLAlchemy engine
    #
    # except psycopg2.Error as e:
    #     print("Error connecting to the database:", e)
    #
    # print('database pull complete', str(datetime.now()).split('.')[0])
    # print(dfs)
    tables_str = ', '.join(dfs)

    return {
        'body': {
            'response_type': 'in_channel',
            'text': 'database pull complete',
            'tables': tables_str,

        }
    }
